// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
// 引入路由
import VueRouter from "vue-router";
import routerConfig from "./router/router.config.js";

import App from "./App";
// 引入elementUI
import ElementUI from "element-ui";
// import "element-ui/lib/theme-chalk/index.css";
// 自定义主题
import "../theme/index.css";
import locale from "element-ui/lib/locale/lang/en";

Vue.config.productionTip = false;
// 使用element-ui
Vue.use(ElementUI, { locale });
// 使用路由
Vue.use(VueRouter);
const router = new VueRouter(routerConfig);

/* eslint-disable no-new */
new Vue({
  router,
  el: "#app",
  render: h => h(App)
  // components: { App },
  // template: "<App/>"
});

// 关于render: h => h(App),的理解
// ES5
// createElement 函数是用来生成 HTML DOM 元素
// render: function (createElement) {
//   return createElement(App);
// }
