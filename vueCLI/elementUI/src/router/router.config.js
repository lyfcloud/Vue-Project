import Hello from "./../components/Hello.vue";
import HelloWorld from "./../components/HelloWorld.vue";

// 路由导航
export default {
  routes: [
    { path: "/", redirect: "/hello" },
    { path: "/hello", component: Hello },
    { path: "/helloworld", component: HelloWorld },
    
  ]
};
